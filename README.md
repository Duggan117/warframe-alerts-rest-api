# Warframe Alerts REST API

## Setup

### Build dependencies

Type `npm install` in the installation directory to install required dependencies.

## How to use

Simply start the server by typing the following command : `node server.js`

## REST API features

The REST API provide some filters :

### with_reward

 * **Type** : `boolean`
 * **Description** : Show only alerts with reward `true` or not `false`.

### from

 * **Type** : `string`
 * **Description** : Return alerts after the specified `id`

### count
 * **Type** : `integer`
 * **Description** : Show `X` latest alerts

Other filters will appear in response to requests.

## Branch strategy

This repository is splitted into following branches :

 * **master** : releases and stable revisions
 * **develop** : new features

## Copyright

License : GPL v2

Read file [COPYING](COPYING.md)
