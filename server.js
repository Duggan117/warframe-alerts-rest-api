/*
 * Copyright (C) 2013 Seldszar <http://www.seldszar.fr/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

var Twitter = require("ntwitter");
var RateLimiter = require("limiter").RateLimiter;
var express = require("express");
var http = require("http");
var winston = require('winston');
var util = require("./util");
var alerts = require("./alerts");

var config = util.extend({
    "server": {
        "hostname": "127.0.0.1",
        "port": 5416,
        "defaults": {
            "count": 25
        },
        "rate_limit": -1
    },
    "twitter": {
        "retrieve_interval": 30000
    },
    "alerts": {
        "min_count": 100
    }
}, require("./config"));

var twit = new Twitter(config.twitter.credentials);
var limiter = new RateLimiter(config.server.rate_limit, "hour", true);
var app = express();
var server = http.createServer(app);

var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({timestamp: true, colorize: true}),
        new (winston.transports.File)({timestamp: true, filename: 'logs/' + Date.now() + '.log', json: false})
    ],
    exitOnError: false
});

alerts.on("add", function(alert) {
    logger.info("New alert found : %s (%s)", alert.mission, alert.location, alert);
});

app.configure(function() {
    app.disable("x-powered-by");
    app.enable("trust proxy");
    app.use(express.bodyParser());
    app.use(express.compress());
    app.use(function(req, res, next) {
        res.header("Cache-Control", "no-cache");
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "X-Requested-With");
        next();
    });
    app.use(function(req, res, next) {
        limiter.removeTokens(1, function(err, remainingRequests) {
            res.header("X-Tokens-Remaining", limiter.getTokensRemaining());
            if (remainingRequests < 0) {
                res.send(429, {
                    "error": "Too Many Requests.",
                    "request": req.originalUrl
                });
            } else {
                next();
            }
        });
    });
    app.use(app.router);
    app.use(function(req, res, next) {
        res.send(404, {
            "error": "Page not found.",
            "request": req.originalUrl
        });
    });
});

function retrieveAlerts() {
    logger.info("Retrieve alerts...");
    var options = {
        user_id: config.twitter.user_id,
        count: config.alerts.min_count
    };
    if (alerts.last && alerts.last.id) {
        options["since_id"] = alerts.last.id;
    }
    twit.getUserTimeline(options, function(err, tweets) {
        if (err) {
            logger.error(err);
        } else {
            alerts.addMany(tweets.reverse());
            alerts.sort();
        }
    });
}

app.get("/alerts", function(req, res) {
    var filteredAlerts = alerts.list;
    var query = req.query;
    if (query.from) {
        var hashReached = false;
        filteredAlerts = filteredAlerts.filter(function(alert) {
            if (alert.id == query.from) {
                hashReached = true;
            }
            return !hashReached;
        });
    }
    if (query.in_progress) {
        var now = new Date();
        filteredAlerts = filteredAlerts.filter(function(alert) {
            return (alert.end > now) == util.parseBool(query.in_progress);
        });
    }
    if (query.with_reward) {
        filteredAlerts = filteredAlerts.filter(function(alert) {
            return util.isDefined(alert.reward) == util.parseBool(query.with_reward);
        });
    }
    filteredAlerts = filteredAlerts.slice(0, query.count || config.server.defaults.count);
    res.send(filteredAlerts);
});

server.listen(config.server.port, config.server.hostname, function() {
    var addr = this.address();
    logger.info("Server listening at http://%s:%d/", addr.address, addr.port);
    setInterval(retrieveAlerts, config.twitter.retrieve_interval);
    retrieveAlerts();
});
